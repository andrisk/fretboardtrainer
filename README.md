# Fretboard Trainer

Mobile application helping users learn notes on a guitar (or other string instrument) fretboard.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/fretboardtrainer.andrisk)

## Building
Fretboard Trainer is built using [clickable](https://clickable.bhdouglass.com/en/latest/).

## License

Copyright (C) 2020, 2023  Andrej Trnkóci

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](http://www.gnu.org/licenses/)