/*
 * Copyright (C) 2020, 2023  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import "common.js" as Common

Canvas
{
    id: fretboardCanvas
    property var stringNotes: [7,2,10,5,0,7]
    property int fretCount: 12
    property var singleMarkerFrets: [3, 5, 7, 9]
    property var doubleMarkerFrets: [12]

    property color backgroundColor: theme.name === "Lomiri.Components.Themes.SuruDark" ?  Qt.rgba(0, 0, 0, 1) : Qt.rgba(1, 1, 1, 1)
    property color noteMarkerFontColor: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(1, 1, 1, 1) : Qt.rgba(0, 0, 0, 1)
    property color correctNoteMarkerBackground: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0,0.5,0,0.9) : Qt.rgba(0.5, 0.9, 0.5, 0.9)
    property color incorrectNoteMarkerBackground: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0.5,0,0,0.9) : Qt.rgba(0.9, 0.5, 0.5, 0.9)
    property color questionMarkerBackground: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0,0,0.5,0.9) : Qt.rgba(0.8, 0.9, 1, 0.9)
    property color fretColor: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0.5, 0.5, 0.5, 1) : Qt.rgba(0.45, 0.40, 0.35, 1)
    property color fretMarkersColor:  theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0.85, 0.85, 0.85, 1) : Qt.rgba(0.05, 0.05, 0.05, 1)
    property color stringColor: Qt.rgba(1, 1, 1, 1)
    property color nutColor: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0.5, 0.25, 0.25, 1) : Qt.rgba(0.2, 0.1, 0.1, 1)
    property color fretboardWoodColor: theme.name === "Lomiri.Components.Themes.SuruDark" ? Qt.rgba(0.12, 0.12, 0.15, 1) : Qt.rgba(0.93, 0.75, 0.45, 1)


    signal addMarker(var fret, var string, var markerType)
    signal clearMarkers()

    onAddMarker:
    {
        privateVariables.markers.push({fret: fret, string: string, markerType: markerType})
        requestPaint()
    }

    onClearMarkers:
    {
        privateVariables.markers.length = 0
        requestPaint()
    }

    QtObject
    {
        id: privateVariables
        property var markers: []
    }

    onPaint:
    {

        //var stringNotes = [7,2,10,5,0,7]
        var stringCount = stringNotes.length
        var stringSize = 0.01
        var fretSize = 0.015

        var ctx = getContext("2d");

        var fretboardData = generateFretboard(ctx, width, height, stringCount, stringSize, fretCount, fretSize, singleMarkerFrets, doubleMarkerFrets)

        privateVariables.markers.forEach(function(currentMarker)
        {
            if (currentMarker.markerType === "CorrectNote")
            {
                drawNoteMarker(ctx, fretboardData, currentMarker.fret, currentMarker.string, stringNotes, true)
            }
            else if (currentMarker.markerType === "IncorrectNote")
            {
                drawNoteMarker(ctx, fretboardData, currentMarker.fret, currentMarker.string, stringNotes, false)
            }
            else if (currentMarker.markerType === "Question")
            {
                drawQuestionMarker(ctx, fretboardData, currentMarker.fret, currentMarker.string)
            }
        });
    }

    function getNoteIdOnFretboard(stringNum, fretNum)
    {
        return (stringNotes[stringNum] + fretNum) % 12
    }



    function drawNoteMarker(context, fretboardData, fretNumber, stringNumber, emptyStringsNotes, isCorrect)
    {
        var noteId = getNoteIdOnFretboard(stringNumber, fretNumber)
        var markerString = Common.noteIdToName(noteId)
        if (isCorrect)
        {
            drawMarker(context, fretboardData, fretNumber, stringNumber, markerString, correctNoteMarkerBackground)
        }
        else
        {
            drawMarker(context, fretboardData, fretNumber, stringNumber, markerString, incorrectNoteMarkerBackground)
        }
    }

    function drawQuestionMarker(context, fretboardData, fretNumber, stringNumber)
    {
        drawMarker(context, fretboardData, fretNumber, stringNumber, "?", questionMarkerBackground);
    }

    function drawMarker(context, fretboardData, fretNumber, stringNumber, markerString, markerColor)
    {

        var startX = fretboardData.fretSize * fretNumber + fretboardData.fretSpacing * fretNumber
        var endX = fretboardData.fretSize * fretNumber + fretboardData.fretSpacing * (fretNumber + 1)

        var startY = fretboardData.stringSize * (stringNumber) + fretboardData.stringSpacing * (stringNumber + 0.5)
        var endY = fretboardData.stringSize * (stringNumber + 1) + fretboardData.stringSpacing * (stringNumber + 1.5)

        var maxWidth = endX - startX
        var maxHeight = endY - startY

        context.textBaseline = "top"
        //context.textAlign = "end"
        var fontSize = (maxHeight * 0.8) //(maxWidth / markerString.length * 1.7) < (maxHeight * 0.9) ?  (maxWidth / markerString.length * 1.7) : (maxHeight * 0.9)
        context.font = fontSize + "px monospace" //sans-serif"

        var textMeasure;
        while ((textMeasure = context.measureText(markerString)).width > maxWidth)
        {
            fontSize --
            context.font = fontSize + "px monospace"
        }

        context.fillStyle = markerColor
        context.fillRect(startX, startY, maxWidth, maxHeight)


        context.fillStyle = noteMarkerFontColor
        context.fillText(markerString, startX + (maxWidth - textMeasure.width)/2, startY + ((maxHeight - fontSize) / 2 ));/* startY + maxWidth / 2 - fontSize / 2*/
     }

    function generateFretboard(context, width, height, stringCount, stringSize, fretCount, fretSize, singleMarkerFrets, doubleMarkerFrets)
    {
        context.reset()

        var actualFretSize = width * fretSize
        var totalFretSpace = actualFretSize * (fretCount + 1)
        var totalSpaceBetweenFrets = width - totalFretSpace
        var fretSpacing = totalSpaceBetweenFrets / (fretCount + 1)

        var actualStringSize =  height * stringSize
        var totalStringSpace = actualStringSize * stringCount
        var totalSpaceBetweenStrings = height - totalStringSpace
        var stringSpacing = totalSpaceBetweenStrings  / (stringCount + 1)

        context.fillStyle = backgroundColor
        context.fillRect(0, 0, width, height);

        context.fillStyle = fretboardWoodColor
        context.fillRect(0, stringSpacing / 2, width, height - stringSpacing)

        context.fillStyle = nutColor

        for (var x = 0; x <= fretCount; x++)
        {
            var fretStartX = fretSpacing * (x+1) + actualFretSize * x
            context.fillRect(fretStartX, stringSpacing / 2, actualFretSize, height - stringSpacing)
            context.fillStyle = fretColor
        }

        context.fillStyle = fretMarkersColor
        //draw fret markers
        singleMarkerFrets.forEach(function(currentFret) {
            if (currentFret <= fretCount)
            {
                //console.log(currentFret);
                var markerSize = fretSpacing < stringSpacing ? fretSpacing / 2 : stringSpacing / 2
                var currentMarkerX = (currentFret + 0.5)  * fretSpacing + currentFret * actualFretSize
                context.ellipse(currentMarkerX - markerSize / 2, (height - markerSize) / 2, markerSize, markerSize)
                context.fill()
            }
        });

        doubleMarkerFrets.forEach(function(currentFret)
        {
            if (currentFret <= fretCount)
            {
                var markerSize = fretSpacing < stringSpacing ? fretSpacing / 2 : stringSpacing / 2
                var doubleMarkerX = (currentFret + 0.5)  * fretSpacing + currentFret * actualFretSize - markerSize / 2
                var doubleMarker1Y = 1.5 * stringSpacing + 1 * actualStringSize - markerSize / 2
                var doubleMarker2Y = height - doubleMarker1Y - markerSize
                context.ellipse( doubleMarkerX, doubleMarker1Y, markerSize, markerSize)
                context.fill()
                context.ellipse( doubleMarkerX, doubleMarker2Y, markerSize, markerSize)
                context.fill()
            }
        });

        context.fillStyle = stringColor;

        for (var y = 0; y < stringCount; y++)
        {
            var stringStartY = stringSpacing * (y+1) + actualStringSize * y
            context.fillRect(0, stringStartY, width, actualStringSize)
        }

        return { width: width,
                 height: height,
                 fretCount: fretCount,
                 fretSize: actualFretSize,
                 fretSpacing: fretSpacing,
                 stringCount: stringCount,
                 stringSize: actualStringSize,
                 stringSpacing: stringSpacing,
                 canvasImage: context.getImageData(0, 0, width, height)
               };
    }
}


