/*
 * Copyright (C) 2020  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "common.js" as Common

GridLayout
{
    //width: parent.width
    rowSpacing: 0
    columnSpacing: 0
    //property int buttonMaxWidth: 75;
    //property int buttonMaxHeight: 35;
    property int buttonMargin: 2;
    //property int maxColumns: (width) / (columnSpacing + buttonMaxWidth + buttonMargin * 2)
    //property int minRows: Math.ceil(12.0 / maxColumns)
    property int numberOfButtons: 12;
    columns: getColumnCount();  //Math.ceil(12.0 / minRows)
    property int rows: numberOfButtons / columns;
    //height: minRows * (buttonMaxHeight + buttonMargin * 2); //(rowSpacing + buttonMaxHeight) * ((12 / columns) + 1)//parent.height - fretboardCanvas.height
    property int buttonWidth: width / columns - buttonMargin * 2;
    property int buttonHeight: height / rows - buttonMargin * 2;

    Repeater
    {
        model: 12
        Button
        {

            Layout.margins: parent.buttonMargin
            Layout.maximumWidth: buttonWidth;//parent.buttonMaxWidth
            Layout.maximumHeight: buttonHeight;//parent.buttonMaxHeight
            Layout.minimumWidth: buttonWidth;
            Layout.minimumHeight: buttonHeight;
            Layout.alignment: Qt.AlignCenter
            text: Common.noteIdToName(index);

            onClicked:
            {
                answer(index);
            }
        }
    }

    function getColumnCount()
    {
        if (width/height < 0.16)
        {
            return 1;
        }
        else if (width/height < 0.66)
        {
            return 2;
        }
        else if (width/height < 1.5)
        {
            return 3;
        }
        else if (width/height < 2.66)
        {
            return 4;
        }
        else if (width/height < 9)
        {
            return 6;
        }
        else
        {
            return 12;
        }
    }
}
