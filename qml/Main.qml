/*
 * Copyright (C) 2020, 2023  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "common.js" as Common

//MainView {
ApplicationWindow {
    id: root
    objectName: 'mainView'

    width: units.gu(45)
    height: units.gu(75)

    property bool canAnswer: false;
    property bool showingAllNotes: false;
    property int currentQuestionFret: 0;
    property int currentQuestionString: 0;

    Timer {
        id: newNoteCountdown
        interval: 3000;
        running: false;
        repeat: false;
        onTriggered: askRandomNote();
    }

    Rectangle
    {
        width: parent.width
        height: parent.height - answerButtons.height
        anchors.bottom: answerButtons.top
        color: theme.name === "Lomiri.Components.Themes.SuruDark" ? "black" : "white"

        FretboardCanvas
        {
            id: fretboard
            width: parent.width
            height: width / 3
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            MouseArea {
                anchors.fill: parent
                onClicked:
                {
                    if (showingAllNotes)
                    {
                        askRandomNote();
                        showingAllNotes = false;
                    }
                    else
                    {
                        newNoteCountdown.stop();
                        canAnswer = false;
                        showAllNotes();
                        showingAllNotes = true;
                    }
                }
            }
        }
    }

    NoteKeyboard
    {
        id: answerButtons
        width: parent.width
        height: (parent.height - fretboard.height) < width * 0.66 ? parent.height - fretboard.height : width * 0.66
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
    }

    function askRandomNote()
    {
        fretboard.clearMarkers();
        currentQuestionFret = Math.floor(Math.random() * (fretboard.fretCount + 1));
        currentQuestionString = Math.floor(Math.random() * (fretboard.stringNotes.length));
        fretboard.addMarker(currentQuestionFret, currentQuestionString, "Question");
        canAnswer = true;
    }

    function answer(noteId)
    {
        if (canAnswer)
        {
            canAnswer = false;
            fretboard.clearMarkers();
            var correctNoteId = fretboard.getNoteIdOnFretboard(currentQuestionString,currentQuestionFret);
            if (noteId === correctNoteId)
            {
                fretboard.addMarker(currentQuestionFret, currentQuestionString, "CorrectNote");
            }
            else
            {
                fretboard.addMarker(currentQuestionFret, currentQuestionString, "IncorrectNote");
            }
            newNoteCountdown.start();
        }
    }

    function showAllNotes()
    {
        fretboard.clearMarkers();
        for(var fret = 0; fret < fretboard.fretCount + 1; fret++)
        {
            for(var stringNum = 0; stringNum < fretboard.stringNotes.length; stringNum++)
            {
                fretboard.addMarker(fret, stringNum, "CorrectNote");
            }
        }
    }

    Component.onCompleted:
    {
        askRandomNote();
        canAnswer = true;
        //theme.name = "Lomiri.Components.Themes.SuruDark"
        //console.log(theme.name)
    }


}
